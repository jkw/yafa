from flask import Flask
from flask import render_template
from flask_login import LoginManager
from flask_login import login_required

app = Flask(__name__, template_folder='static')

app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'derpmaherp'
app.static_folder = 'static'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'page_login'

site_theme = 'bootstrap4'
admin_theme = 'clever'


@app.route('/')
def page_index(theme=site_theme):
    return render_template('bootstrap4/index.html', theme=theme)


@app.route('/admin')
@login_required
def page_admin(theme=admin_theme):
    return render_template('clever/index.html', theme=theme)


@app.route('/login')
def page_login(theme=admin_theme):
    return render_template('clever/pages-login.html', theme=theme)


if __name__ == "__main__":
    app.run()
